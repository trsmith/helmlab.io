{{- if .Values.enabled }}
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: {{ template "fullname" . }}
  labels:
    app: {{ template "name" . }}
    chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        app: {{ template "name" . }}
        release: {{ .Release.Name }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yml") . | sha256sum }}
    spec:
      securityContext:
        runAsUser: 1000
        fsGroup: 1000
      initContainers:
        - name: configure
          command: ['sh', '/config/configure']
          image: busybox
          volumeMounts:
          - name: unicorn-config
            mountPath: /config
            readOnly: true
          - name: init-unicorn-secrets
            mountPath: /init-config
            readOnly: true
          - name: unicorn-secrets
            mountPath: /init-secrets
            readOnly: false
        - name: {{ .Chart.Name }}-dependencies
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          args:
            - /scripts/wait-for-deps
          env:
            - name: GITALY_FEATURE_DEFAULT_ON
              value: "1"
            - name: CONFIG_TEMPLATE_DIRECTORY
              value: '/var/opt/gitlab/templates'
            - name: CONFIG_DIRECTORY
              value: '/var/opt/gitlab/config/gitlab/'
          volumeMounts:
            - name: unicorn-config
              mountPath: '/var/opt/gitlab/templates'
            - name: unicorn-secrets
              mountPath: '/etc/gitlab'
              readOnly: true
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - containerPort: {{ .Values.service.internalPort }}
              name: unicorn
            - containerPort: {{ .Values.service.workhorseInternalPort }}
              name: workhorse
          env:
            - name: GITALY_FEATURE_DEFAULT_ON
              value: "1"
            - name: CONFIG_TEMPLATE_DIRECTORY
              value: '/var/opt/gitlab/templates'
            - name: CONFIG_DIRECTORY
              value: '/var/opt/gitlab/config/gitlab/'
          volumeMounts:
            - name: unicorn-config
              mountPath: '/var/opt/gitlab/templates'
            - name: unicorn-secrets
              mountPath: '/etc/gitlab'
              readOnly: true
          livenessProbe:
            exec:
              command:
              - /scripts/healthcheck
            initialDelaySeconds: 20
            timeoutSeconds: 30
            periodSeconds: 60
          # readinessProbe:
          #   httpGet:
          #     path: /
          #     port: {{ .Values.service.internalPort }}
          resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
      - name: unicorn-config
        configMap:
          name: {{ template "fullname" . }}
      - name: init-unicorn-secrets
        projected:
          defaultMode: 0400
          sources:
          - secret:
              name: {{ .Values.shell.authToken.secret }}
              items:
                - key: {{ default "secret" .Values.shell.authToken.key }}
                  path: shell/.gitlab_shell_secret
          - secret:
              name: {{ .Values.gitaly.authToken.secret }}
              items:
                - key: {{ default "token" .Values.gitaly.authToken.key }}
                  path: gitaly/gitaly_token
          - secret:
              name: {{ .Values.redis.password.secret }}
              items:
                - key: {{ .Values.redis.password.key }}
                  path: redis/password
          - secret:
              name: {{ .Values.psql.password.secret }}
              items:
                - key: {{ .Values.psql.password.key }}
                  path: postgres/psql-password
          - secret:
              name: {{ .Values.registry.certificate.secret }}
              items:
                - key: {{ .Values.registry.certificate.key }}
                  path: registry/gitlab-registry.key
      - name: unicorn-secrets
        emptyDir:
          medium: "Memory"
    {{- if .Values.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 8 }}
    {{- end }}
{{- end }}
