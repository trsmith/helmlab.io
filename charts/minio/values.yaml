## Set default image, imageTag, and imagePullPolicy.
## Distributed Minio ref: https://docs.minio.io/docs/distributed-minio-quickstart-guide
##
image: "minio/minio"
imageTag: "RELEASE.2017-12-28T01-21-00Z"
imagePullPolicy: "Always"

## Enable/Disable
enabled: true
## Control the InitContainer behaviors.
init:
  image: "busybox"
  tag: "latest"
  pullPolicy: "IfNotPresent"
  script: |-
    sed -e 's@ACCESS_KEY@'"$(cat /config/accesskey)"'@' -e 's@SECRET_KEY@'"$(cat /config/secretkey)"'@' /config/config.json > /minio/config.json

credentials:
  secret: gitlab-minio

## Set default Minio config file path, volume mount path and
## number of nodes (only used for Minio distributed mode)
## Distributed Minio ref: https://docs.minio.io/docs/distributed-minio-quickstart-guide
##
configPath: ""
mountPath: "/export"
replicas: 4

## loadBalancerIP for the Minio Service (optional, cloud specific)
## ref: http://kubernetes.io/docs/user-guide/services/#type-loadbalancer
##
# minioLoadBalancerIP:

## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  enabled: true

  ## minio data Persistent Volume Storage Class
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  # storageClass: "-"
  accessMode: ReadWriteOnce
  size: 10Gi

  ## If subPath is set mount a sub folder of a volume instead of the root of the volume.
  ## This is especially handy for volume plugins that don't natively support sub mounting (like glusterfs).
  ##
  subPath: ""

  ## if volumeName is set, use this existing PersistentVolume
  # volumeName:

## Expose the Minio service to be accessed from outside the cluster (LoadBalancer service).
## or access it from within the cluster (ClusterIP service). Set the service type and the port to serve it.
## ref: http://kubernetes.io/docs/user-guide/services/
##
serviceType: ClusterIP
servicePort: 9000

## Node labels for pod assignment
## Ref: https://kubernetes.io/docs/user-guide/node-selection/
##
nodeSelector: {}

## Configure resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources:
  requests:
    memory: 256Mi
    cpu: 250m

## Create a buckets after minio install
## An array of items, with name, policy, and purge
defaultBuckets:
  ## If enabled, must be a string with length > 0
  # - name: example
  ## Can be one of none|download|upload|public, defaults to `none`
  #   policy: none
  ## Can be omitted. If present, and `true`, will `rm -rf` the bucket
  #   purge: false

## https://docs.minio.io/docs/minio-bucket-notification-guide
##
minioConfig:
  region: "us-east-1"
  browser: "on"
  domain: ""
  logger:
    console:
      enable: true
    file:
      enable: false
      filename: ""
  aqmp:
    enable: false
    url: ""
    exchange: ""
    routingKey: ""
    exchangeType: ""
    deliveryMode: 0
    mandatory: false
    immediate: false
    durable: false
    internal: false
    noWait: false
    autoDeleted: false
  nats:
    enable: false
    address: ""
    subject: ""
    username: ""
    password: ""
    token: ""
    secure: false
    pingInterval: 0
    enableStreaming: false
    clusterID: ""
    clientID: ""
    async: false
    maxPubAcksInflight: 0
  elasticsearch:
    enable: false
    format: "namespace"
    url: ""
    index: ""
  redis:
    enable: false
    format: "namespace"
    address: ""
    password: ""
    key: ""
  postgresql:
    enable: false
    format: "namespace"
    connectionString: ""
    table: ""
    host: ""
    port: ""
    user: ""
    password: ""
    database: ""
  kafka:
    enable: false
    brokers: "null"
    topic: ""
  webhook:
    enable: false
    endpoint: ""
  mysql:
    enable: false
    format: "namespace"
    dsnString: ""
    table: ""
    host: ""
    port: ""
    user: ""
    password: ""
    database: ""
  mqtt:
    enable: false
    broker: ""
    topic: ""
    qos: 0
    clientId: ""
    username: ""
    password: ""
networkPolicy:
  enabled: false
  allowExternal: true
